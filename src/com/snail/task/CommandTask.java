/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.snail.task;

import com.snail.beans.CmdResult;
import com.snail.cfg.XLogger;
import com.snail.main.XTask;
import it.sauronsoftware.cron4j.TaskExecutionContext;
import org.jsoup.nodes.Element;

/**
 *
 * @author pm
 */
public class CommandTask extends XTask {

    

    public CommandTask(Element full_cfg,Element cfg, Element module) {
        super(full_cfg,cfg, module);
    }

    @Override
    public void onExecute(TaskExecutionContext tec) throws RuntimeException {
        String cmd = getText(cfg.select(">cmd").first());
        if (!cmd.isEmpty()) {
            CmdResult result = runCommand(cfg.select(">cmd").first());
            if (result.isSuccess()) {
                if (checkFlag(cfg.select(">success>flag").first(), result.getOutput())) {
                    onSuccess(result.getOutput());
                } else {
                    onError("1", result.getOutput());
                }
            } else {
                onError("2", result.getOutput());
            }
        }
    }

}
