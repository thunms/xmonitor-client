/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.snail.task;

import com.snail.cfg.XLogger;
import com.snail.main.XTask;
import com.woniu.utils.net.http.HttpGetForm;
import com.woniu.utils.net.http.HttpGetPostResponse;
import com.woniu.utils.net.http.HttpPostForm;

import it.sauronsoftware.cron4j.TaskExecutionContext;
import org.apache.http.HttpResponse;
import org.jsoup.nodes.Element;

/**
 *
 * @author pm
 */
public class HttpTask extends XTask {

    private final String url, method;
    private final int timeout;

    public HttpTask(Element full_cfg, Element cfg, Element module) {
        super(full_cfg, cfg, module);
        url = cfg.attr("url");
        method = cfg.attr("method");
        timeout = Integer.parseInt(cfg.attr("timeout"));
    }

    @Override
    public void onExecute(TaskExecutionContext tec) throws RuntimeException {

        HttpGetPostResponse response;
        XLogger.logger.trace("http " + method + " : " + url);
        if ("post".equalsIgnoreCase(method)) {
            HttpPostForm form = new HttpPostForm();
            if (cfg.select(">form>item").size() > 0) {
                for (Element item : cfg.select(">form>item")) {
                    form.add(item.attr("name"), getText(item));
                }
            }
            if (url.startsWith("https://")) {
                response = https(timeout).doPost(url, form);
            } else {
                response = http(timeout).doPost(url, form);
            }
        } else {
            HttpGetForm form = new HttpGetForm();
            if (cfg.select(">form>item").size() > 0) {
                for (Element item : cfg.select(">form>item")) {
                    form.add(item.attr("name"), getText(item));
                }
            }
            if (url.startsWith("https://")) {
                response = https(timeout).doGet(url, form);
            } else {
                response = http(timeout).doGet(url, form);
            }
        }

        XLogger.logger.trace("http " + method + " : " + url + " done");
        HttpResponse response1 = response.getResponse();
        String http_code = response1 != null ? String.valueOf(response1.getStatusLine().getStatusCode()) : "";
        String http_text = response.getString(true);
        String success_code = cfg.select(">success").attr("code");
        if (success_code.equals(http_code)) {
            if (checkFlag(cfg.select(">success>flag").first(), http_text)) {
                //okay
                onSuccess(http_text);
            } else {
                //error text
                onError("1", http_text);
            }
        } else {
            if (response1 != null) {
                //error code
                onError("3", http_code);
            } else {
                //error timeout
                onError("5", "");
            }
        }
    }

}
