#XMonitor-Client
开源的Linux服务器监控，数据采集，Linux服务器运维的有力助手。支持：http监控，进程监控，MySQL监控，TCP监控，PING监控，命令结果监控。灵活使用会有意想不到的效果。
使用只要下载:xmonitor-client_x.x.x.tar.gz压缩文件即可.
#XMonitor-Client手册 V1.0.2
  
**0.相关说明**  
    环境要求:Linux系统,jdk1.7及以上  
    配置文件是config/client.xml  
    日志配置文件config/logback-log.xml,基于logback。正式使用的时候可以注释掉appender-ref ref="STDOUT"  
    启动,关闭,查看状态:  
    cd {xmonitor-client目录}  
    启动:/bin/bash bin/xmointor.sh start  
    关闭:/bin/bash bin/xmointor.sh stop  
    查看状态:/bin/bash bin/xmointor.sh status  
    调试:/bin/bash bin/xmointor.sh debug   
  
**1.**xmonitor-client配置是模块化的结构。每个module模块有一个或多个task任务组成。  
  
**2.**每个task任务可以像linux的crontab计划任务一样随意的定期执行，语法完全和crontab一样。  
  可以通过task的cron属性定义执行周期。  

**3.**关于“task任务的输出”的解释，由于有很多种任务，大部分任务都是有输出的，  
  比如http任务，task的输出结果是html。命令结果监控任务，task输出结果就是命令的执行输出结果。  
  凡是这种task中会产生输出的结果在XMonitor-Client里称为“task任务的输出”。  
  
**4.**可以通过status属性开启或者关闭一个module和task，status为close时module或者task不再起作用，默认为：open  
  
**5.**每个task任务有success成功，error失败，recovery恢复三个状态。  
  
**6.**每个task任务状态发生后都可以把task任务输出的数据和其它信息，  
  使用email或者http的方式发送到自定义的服务端。还可以执行一些系统命令。  
  
**7.**所有超时时间单位均为：毫秒，1秒=1000毫秒  
  
**8.success模块介绍**  
  success模块是当输出结果“匹配成功”后触发的模块。  
  “匹配成功”可以是对flag的匹配或者模块里的定义的其它规则匹配成功。  
  success模块被触发后可以通过mailto、request和cmd模块，发送邮件，http请求，执行系统命令。  
  
**9.error模块介绍**  
  error模块是当输出结果“匹配失败”后触发的模块。  
  “匹配失败”可以是对flag的匹配或者模块里的定义的其它规则匹配失败。  
  error模块被触发后可以通过mailto、request和cmd模块，发送邮件，http请求，执行系统命令。  
  error模块有两个属性：  
  warn_count   ：task匹配失败但是不一定会触发失败模块，只有失败的次数超过warn_count次失败模块就会被触发。  
  skip_count   ：失败模块被触发后，如果持续失败，那么会跳过skip_count次然后再次触发失败模块，避免短时间内频繁的错误警告。  
  max_warn_count：如果长时间内错误一直持续，error模块最多会被触发max_warn_count次，以后不会再触发，直到故障恢复。  
                  这样当故障短时间无法解决，能避免频繁的故障通知。  
  
**10.recovery模块介绍**   
  recovery模块是当任务由**失败状态转为成功状态**的时候会被触发一次。这里可以进行“服务恢复正常”时要做的事情  
  “匹配成功”可以是对flag的匹配或者模块里的定义的其它规则匹配成功。  
  recovery模块被触发后可以通过mailto、request和cmd模块，发送邮件，http请求，执行系统命令。  
  
**11.flag模块介绍**  
  flag是对task的输出结果进行判断是否正常的模块，如果正常，则触发success成功模块。  
  假定flag包含的值是value，task输出是output  
  flag的type属性有：  
  emptpy  ：output为空则成功  
  include ：output中包含value则成功  
  exclude ：output中不包含value则成功  
  any     ：output任意内容都成功  
  reg     ：output匹配正则表达式value则成功  
  http    ：flag里面的request模块成功则flag成功  
  number  ：output是整数或者小数则成功  
  &gt;=      ：output >= value 成立则成功  
  =       ：output == value 成立则成功  
  <=      ：output <= value 成立则成功  
  **flag可以在success和request中使用。**  
  
**12.mailto模块介绍**  
  mailto模块用来发邮件，可以把任务的输出发送给一个或者多个to收件人。  
  关于发邮件：  
  global里面有个mailto发邮件的全局设置，这里可以定义一个或者多个全局的to收件人。  
  client里面有个email发邮件用到的帐号配置,可以定一个或者多个account。每个account都有个id，id对应mailto的account_id，告诉mailto使用哪个账号  
  
**13.request模块介绍**  
  request模块用来发http请求，支持http和https，可以把任务的输出发送到指定的URL。  
  可以  
  1.通过属性method指定是get还是post  
  2.通过属性code定义返回的http状态码用来判断是否请求成功  
  3.还可以通过定义一个flag对提交数据指定的URL的输出结果进行匹配作为判断是否请求成功  
  4.可以定义一个form，指定要发送的数据，form可以看作一个html表单，  
    item就是一个表单元素，name属性是get或者post中使用的参数名称，  
    item包含的值就是参数数据。  
  
**14.cmd模块介绍**  
  cmd模块用来执行系统命令。可以通过timeout属性定义命令执行超时时间。0意味着不超时。  
  cmd标记包含的之就是要执行的命令,命令可以是串连的,比如:service  mysql status || service  mysql start
  
**15.变量介绍**  
request的form里的item以及mailto里的title和content里面可以用的变量有：  
　　　{output}　　　　　: task输出的内容  
　　　{code}　　　　　　: 错误状态码：  
　　　　　　　　　　　　　　　　　　1：匹配规则失败  
　　　　　　　　　　　　　　　　　　2：命令执行失败  
　　　　　　　　　　　　　　　　　　3：匹配状态码失败  
　　　　　　　　　　　　　　　　　　4：主机地址不存在  
　　　　　　　　　　　　　　　　　　5：操作超时  
　　　　　　　　　　　　　　　　　　6：连接失败  
　　　{time}　　　　　　 :  task发生的时间戳，单位秒  
　　　{time_str}　　　　 :  task发生的时间信息，格式：2014-03-03 12:34:00  
　　　{err_time}　　　　 :  恢复时，错误持续的时间，单位秒  
　　　{err_time_str}　　:  恢复时，错误持续的时间信息，格式：x天x时x分x秒  

**16.config/clinet.xml配置文件结构图**  
![配置文件结构图](http://git.oschina.net/snail/xmonitor-client/raw/master/config_structure.png)  

